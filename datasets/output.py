from glob import glob
import re
import sys

try:
    import pandas as pd
except ImportError:
    print("""Pandas needs to be installed. You can run in a terminal :
	    python3 -m pip install pandas
	    """)
    exit()


def build_txt_from_conllu_format_files(folder_path, data_metadata):
    list_of_files = glob(f"{folder_path}/*.conllu")
    data = pd.read_csv(data_metadata, sep="\t")
    for file in list_of_files:
        file_name = re.search(r".+(comment_.+).conllu", file)
        row = data.loc[data['id'] == file_name.group(1)]
        row_align = row["alignment"].to_string(index=False)
        row_lan = row["lan"].to_string(index=False)
        print(f"<{file_name.group(1)} alignment='{row_align}' langue='{row_lan}'>")
        input_file = open(file, mode='r')
        print(input_file.read())
        print(f"</{file_name.group(1)}>")
        input_file.close()


folder_path = sys.argv[1]
data_metadata = sys.argv[2]
get_the_output = build_txt_from_conllu_format_files(folder_path, data_metadata)
