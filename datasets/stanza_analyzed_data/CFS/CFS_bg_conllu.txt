<comment_2959 alignment='In favor' langue='bg'>
# text = Подкрепям!
# sent_id = 0
1	Подкрепям	подкрепям	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=0|end_char=9|ner=O
2	!	!	PUNCT	punct	_	1	punct	_	start_char=9|end_char=10|ner=O


</comment_2959>
<comment_182194 alignment='In favor' langue='bg'>
# text = Това би било чудесно.
# sent_id = 0
1	Това	този	PRON	Pde-os-n	Case=Nom|Gender=Neut|Number=Sing|PronType=Dem	4	nsubj	_	start_char=0|end_char=4|ner=O
2	би	съм	AUX	Vxitu-o3s	Aspect=Imp|Mood=Cnd|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	4	aux	_	start_char=5|end_char=7|ner=O
3	било	съм	AUX	Vxitcat-sni	Aspect=Imp|Definite=Ind|Gender=Neut|Mood=Ind|Number=Sing|VerbForm=Part|Voice=Act	4	cop	_	start_char=8|end_char=12|ner=O
4	чудесно	чудесен	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	0	root	_	start_char=13|end_char=20|ner=O
5	.	.	PUNCT	punct	_	4	punct	_	start_char=20|end_char=21|ner=O

# text = Първото чуждоезично предаване на българското национално радио е било именно на есперанто през 1936 година.
# sent_id = 1
1	Първото	пръв	ADJ	Monsd	Definite=Def|Degree=Pos|Gender=Neut|NumType=Ord|Number=Sing	3	amod	_	start_char=22|end_char=29|ner=O
2	чуждоезично	чуждоезичен	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	3	amod	_	start_char=30|end_char=41|ner=O
3	предаване	предаване	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	12	nsubj	_	start_char=42|end_char=51|ner=O
4	на	на	ADP	R	_	7	case	_	start_char=52|end_char=54|ner=O
5	българското	български	ADJ	Ansd	Definite=Def|Degree=Pos|Gender=Neut|Number=Sing	7	amod	_	start_char=55|end_char=66|ner=O
6	национално	национален	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	7	amod	_	start_char=67|end_char=77|ner=O
7	радио	радио	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	3	nmod	_	start_char=78|end_char=83|ner=O
8	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	12	aux	_	start_char=84|end_char=85|ner=O
9	било	съм	AUX	Vxitcat-sni	Aspect=Imp|Definite=Ind|Gender=Neut|Mood=Ind|Number=Sing|VerbForm=Part|Voice=Act	12	cop	_	start_char=86|end_char=90|ner=O
10	именно	именно	ADV	Dd	Degree=Pos	12	advmod	_	start_char=91|end_char=97|ner=O
11	на	на	ADP	R	_	12	case	_	start_char=98|end_char=100|ner=O
12	есперанто	есперант	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	0	root	_	start_char=101|end_char=110|ner=O
13	през	през	ADP	R	_	15	case	_	start_char=111|end_char=115|ner=O
14	1936	1936	ADJ	Mofsi	Definite=Ind|Degree=Pos|Gender=Fem|NumType=Ord|Number=Sing	15	amod	_	start_char=116|end_char=120|ner=O
15	година	година	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	12	nmod	_	start_char=121|end_char=127|ner=O
16	.	.	PUNCT	punct	_	12	punct	_	start_char=127|end_char=128|ner=O

# text = Можете да видите тук: https://bg.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B4%D0%B8%D0%BE_%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D0%B8%D1%8F
# sent_id = 2
1	Можете	мога	VERB	Vpiif-r2p	Aspect=Imp|Mood=Ind|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=129|end_char=135|ner=O
2	да	да	AUX	Tx	_	3	aux	_	start_char=136|end_char=138|ner=O
3	видите	видя-(се)	VERB	Vpptf-r2p	Aspect=Perf|Mood=Ind|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin|Voice=Act	1	xcomp	_	start_char=139|end_char=145|ner=O
4	тук	там	ADV	Pdl	PronType=Dem	3	obj	_	start_char=146|end_char=149|ner=O
5	:	:	PUNCT	punct	_	6	punct	_	start_char=149|end_char=150|ner=O
6	https://bg.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B4%D0%B8%D0%BE_%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D0%B8%D1%8F	https://bg.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B4%D0%B8%D0%BE_%D0%91%D1%8A%D0%BB%D0%B3%D0%B0%D1%80%D0%B8%D1%8F	PUNCT	punct	_	1	obl	_	start_char=151|end_char=260|ner=O


</comment_182194>
<comment_132663 alignment='In favor' langue='bg'>
# text = Звучи много разумно.
# sent_id = 0
1	Звучи	звуча	VERB	Vpiif-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=0|end_char=5|ner=O
2	много	много	ADV	Md-pi	Definite=Ind|Degree=Pos|NumType=Card|Number=Plur	3	advmod	_	start_char=6|end_char=11|ner=O
3	разумно	разумно	ADV	Dm	Degree=Pos	1	advmod	_	start_char=12|end_char=19|ner=O
4	.	.	PUNCT	punct	_	1	punct	_	start_char=19|end_char=20|ner=O


</comment_132663>
<comment_2986 alignment='In favor' langue='bg'>
# text = Подкрепям!!!
# sent_id = 0
1	Подкрепям	подкрепям	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=1|end_char=10|ner=O
2	!	!	PUNCT	punct	_	1	punct	_	start_char=10|end_char=11|ner=O
3	!	!	PUNCT	punct	_	1	punct	_	start_char=11|end_char=12|ner=O
4	!	!	PUNCT	punct	_	1	punct	_	start_char=12|end_char=13|ner=O


</comment_2986>
<comment_196352 alignment='In favor' langue='bg'>
# text = Подкрепям коментара на David Bail
# sent_id = 0
1	Подкрепям	подкрепям	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=0|end_char=9|ner=O
2	коментара	коментар	NOUN	Ncmsh	Definite=Def|Gender=Masc|Number=Sing	1	obj	_	start_char=10|end_char=19|ner=O
3	на	на	ADP	R	_	4	case	_	start_char=20|end_char=22|ner=O
4	David	david	PROPN	Npmsi	Definite=Ind|Gender=Masc|Number=Sing	2	nmod	_	start_char=23|end_char=28|ner=O
5	Bail	bail	PROPN	Npmsi	Definite=Ind|Gender=Masc|Number=Sing	4	flat	_	start_char=29|end_char=33|ner=O


</comment_196352>
<comment_186814 alignment='In favor' langue='bg'>
# text = Много харесвам идеята.
# sent_id = 0
1	Много	много	ADV	Md-pi	Definite=Ind|Degree=Pos|NumType=Card|Number=Plur	2	advmod	_	start_char=0|end_char=5|ner=O
2	харесвам	харесвам-(се)	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=6|end_char=14|ner=O
3	идеята	идея	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	2	obj	_	start_char=15|end_char=21|ner=O
4	.	.	PUNCT	punct	_	2	punct	_	start_char=21|end_char=22|ner=O

# text = Точно си говорехме с колегата в кабинета ми колко е трудно да гледаш френски филм, например, след премиерата.
# sent_id = 1
1	Точно	точно	ADV	Dd	Degree=Pos	3	advmod	_	start_char=23|end_char=28|ner=O
2	си	свой	PRON	Ppxtd	Case=Dat|PronType=Prs|Reflex=Yes	3	expl	_	start_char=29|end_char=31|ner=O
3	говорехме	говоря	VERB	Vpitf-m1p	Aspect=Imp|Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin|Voice=Act	0	root	_	start_char=32|end_char=41|ner=O
4	с	с	ADP	R	_	5	case	_	start_char=42|end_char=43|ner=O
5	колегата	колега	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	3	iobj	_	start_char=44|end_char=52|ner=O
6	в	в	ADP	R	_	7	case	_	start_char=53|end_char=54|ner=O
7	кабинета	кабинет	NOUN	Ncmsh	Definite=Def|Gender=Masc|Number=Sing	5	nmod	_	start_char=55|end_char=63|ner=O
8	ми	аз	PRON	Psot--1	Person=1|Poss=Yes|PronType=Prs	7	det	_	start_char=64|end_char=66|ner=O
9	колко	колко	ADV	Piq	NumType=Card|PronType=Int	11	advmod	_	start_char=67|end_char=72|ner=O
10	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	11	cop	_	start_char=73|end_char=74|ner=O
11	трудно	трудно	ADV	Dm	Degree=Pos	3	conj	_	start_char=75|end_char=81|ner=O
12	да	да	AUX	Tx	_	13	aux	_	start_char=82|end_char=84|ner=O
13	гледаш	гледам	VERB	Vpitf-r2s	Aspect=Imp|Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin|Voice=Act	11	csubj	_	start_char=85|end_char=91|ner=O
14	френски	френски	ADJ	Amsi	Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	15	amod	_	start_char=92|end_char=99|ner=O
15	филм	филм	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	13	obj	_	start_char=100|end_char=104|ner=O
16	,	,	PUNCT	punct	_	17	punct	_	start_char=104|end_char=105|ner=O
17	например	например	ADV	Dd	Degree=Pos	20	advmod	_	start_char=106|end_char=114|ner=O
18	,	,	PUNCT	punct	_	20	punct	_	start_char=114|end_char=115|ner=O
19	след	след	ADP	R	_	20	case	_	start_char=116|end_char=120|ner=O
20	премиерата	премиера	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	13	obl	_	start_char=121|end_char=131|ner=O
21	.	.	PUNCT	punct	_	3	punct	_	start_char=131|end_char=132|ner=O

# text = Къде да отида и да платя да го гледам легално?
# sent_id = 2
1	Къде	къде	ADV	Pil	PronType=Int	3	obj	_	start_char=133|end_char=137|ner=O
2	да	да	AUX	Tx	_	3	aux	_	start_char=138|end_char=140|ner=O
3	отида	отида-(си)	VERB	Vppif-r1s	Aspect=Perf|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=141|end_char=146|ner=O
4	и	и	CCONJ	Cp	_	6	cc	_	start_char=147|end_char=148|ner=O
5	да	да	AUX	Tx	_	6	aux	_	start_char=149|end_char=151|ner=O
6	платя	платя	VERB	Vpptf-r1s	Aspect=Perf|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	3	conj	_	start_char=152|end_char=157|ner=O
7	да	да	AUX	Tx	_	9	aux	_	start_char=158|end_char=160|ner=O
8	го	аз	PRON	Ppetas3m	Case=Acc|Gender=Masc|Number=Sing|Person=3|PronType=Prs	9	obj	_	start_char=161|end_char=163|ner=O
9	гледам	гледам	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	6	ccomp	_	start_char=164|end_char=170|ner=O
10	легално	легално	ADV	Dm	Degree=Pos	9	advmod	_	start_char=171|end_char=178|ner=O
11	?	?	PUNCT	punct	_	3	punct	_	start_char=178|end_char=179|ner=O

# text = Филмите са много добър начин за опознаване на културата и съответно за сближаване.
# sent_id = 3
1	Филмите	филм	NOUN	Ncfpd	Definite=Def|Gender=Fem|Number=Plur	5	nsubj	_	start_char=180|end_char=187|ner=O
2	са	съм	AUX	Vxitf-r3p	Aspect=Imp|Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	5	cop	_	start_char=188|end_char=190|ner=O
3	много	много	ADV	Md-pi	Definite=Ind|Degree=Pos|NumType=Card|Number=Plur	4	advmod	_	start_char=191|end_char=196|ner=O
4	добър	добър	ADJ	Amsi	Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	5	amod	_	start_char=197|end_char=202|ner=O
5	начин	начин	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	0	root	_	start_char=203|end_char=208|ner=O
6	за	за	ADP	R	_	7	case	_	start_char=209|end_char=211|ner=O
7	опознаване	опознаване	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	5	nmod	_	start_char=212|end_char=222|ner=O
8	на	на	ADP	R	_	9	case	_	start_char=223|end_char=225|ner=O
9	културата	култура	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	7	nmod	_	start_char=226|end_char=235|ner=O
10	и	и	CCONJ	Cp	_	11	cc	_	start_char=236|end_char=237|ner=O
11	съответно	съответно	ADV	Dd	Degree=Pos	5	advmod	_	start_char=238|end_char=247|ner=O
12	за	за	ADP	R	_	13	case	_	start_char=248|end_char=250|ner=O
13	сближаване	сближаване	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	11	obl	_	start_char=251|end_char=261|ner=O
14	.	.	PUNCT	punct	_	5	punct	_	start_char=261|end_char=262|ner=O

# text = Искам да гледам финландско, хърватско или естонско кино, но къде?
# sent_id = 4
1	Искам	искам	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=263|end_char=268|ner=O
2	да	да	AUX	Tx	_	3	aux	_	start_char=269|end_char=271|ner=O
3	гледам	гледам	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	1	ccomp	_	start_char=272|end_char=278|ner=O
4	финландско	финландски	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	9	amod	_	start_char=279|end_char=289|ner=O
5	,	,	PUNCT	punct	_	6	punct	_	start_char=289|end_char=290|ner=O
6	хърватско	хърватски	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	4	conj	_	start_char=291|end_char=300|ner=O
7	или	или	CCONJ	Cp	_	8	cc	_	start_char=301|end_char=304|ner=O
8	естонско	естонски	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	4	conj	_	start_char=305|end_char=313|ner=O
9	кино	кино	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	3	obj	_	start_char=314|end_char=318|ner=O
10	,	,	PUNCT	punct	_	11	punct	_	start_char=318|end_char=319|ner=O
11	но	но	CCONJ	Cc	_	12	cc	_	start_char=320|end_char=322|ner=O
12	къде	къде	ADV	Pil	PronType=Int	1	conj	_	start_char=323|end_char=327|ner=O
13	?	?	PUNCT	punct	_	1	punct	_	start_char=327|end_char=328|ner=O

# text = Единствената възможност за сега са фестивалите.
# sent_id = 5
1	Единствената	единствен	ADJ	Afsd	Definite=Def|Degree=Pos|Gender=Fem|Number=Sing	2	amod	_	start_char=329|end_char=341|ner=O
2	възможност	възможност	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	0	root	_	start_char=342|end_char=352|ner=O
3	за	за	ADP	R	_	4	case	_	start_char=353|end_char=355|ner=O
4	сега	сега	ADV	Dt	Degree=Pos	2	advmod	_	start_char=356|end_char=360|ner=O
5	са	съм	AUX	Vxitf-r3p	Aspect=Imp|Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	2	cop	_	start_char=361|end_char=363|ner=O
6	фестивалите	фестивал	NOUN	Ncmpd	Definite=Def|Gender=Masc|Number=Plur	2	nsubj	_	start_char=364|end_char=375|ner=O
7	.	.	PUNCT	punct	_	2	punct	_	start_char=375|end_char=376|ner=O

# text = А нашето кино не е продукт, който си избива парите с билети, а повече изкуство.
# sent_id = 6
1	А	а	CCONJ	Cp	_	6	cc	_	start_char=377|end_char=378|ner=O
2	нашето	наш	DET	Pszl-s1nd	Definite=Def|Gender=Neut|Number=Sing|Person=1|Poss=Yes|PronType=Prs	3	det	_	start_char=379|end_char=385|ner=O
3	кино	кино	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	6	nsubj	_	start_char=386|end_char=390|ner=O
4	не	не	PART	Tn	Polarity=Neg	6	advmod	_	start_char=391|end_char=393|ner=O
5	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	6	cop	_	start_char=394|end_char=395|ner=O
6	продукт	продукт	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	0	root	_	start_char=396|end_char=403|ner=O
7	,	,	PUNCT	punct	_	10	punct	_	start_char=403|end_char=404|ner=O
8	който	който	PRON	Pre-os-m	Case=Nom|Gender=Masc|Number=Sing|PronType=Rel	10	nsubj	_	start_char=405|end_char=410|ner=O
9	си	свой	PRON	Ppxtd	Case=Dat|PronType=Prs|Reflex=Yes	10	expl	_	start_char=411|end_char=413|ner=O
10	избива	избивам	VERB	Vpitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	6	acl:relcl	_	start_char=414|end_char=420|ner=O
11	парите	пара	NOUN	Ncfpd	Definite=Def|Gender=Fem|Number=Plur	10	obj	_	start_char=421|end_char=427|ner=O
12	с	с	ADP	R	_	13	case	_	start_char=428|end_char=429|ner=O
13	билети	билет	NOUN	Ncmpi	Definite=Ind|Gender=Masc|Number=Plur	10	obl	_	start_char=430|end_char=436|ner=O
14	,	,	PUNCT	punct	_	15	punct	_	start_char=436|end_char=437|ner=O
15	а	а	CCONJ	Cp	_	17	cc	_	start_char=438|end_char=439|ner=O
16	повече	повече	ADV	Md-pi	Definite=Ind|Degree=Pos|NumType=Card|Number=Plur	17	advmod	_	start_char=440|end_char=446|ner=O
17	изкуство	изкуство	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	13	conj	_	start_char=447|end_char=455|ner=O
18	.	.	PUNCT	punct	_	6	punct	_	start_char=455|end_char=456|ner=O


</comment_186814>
<comment_6965 alignment='In favor' langue='bg'>
# text = Напълно поддържам идеята за въвеждане на безусловен основен доход в ЕС.
# sent_id = 0
1	Напълно	напълно	ADV	Dq	Degree=Pos	2	advmod	_	start_char=0|end_char=7|ner=O
2	поддържам	поддържам	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=8|end_char=17|ner=O
3	идеята	идея	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	2	obj	_	start_char=18|end_char=24|ner=O
4	за	за	ADP	R	_	5	case	_	start_char=25|end_char=27|ner=O
5	въвеждане	въвеждане	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	3	nmod	_	start_char=28|end_char=37|ner=O
6	на	на	ADP	R	_	9	case	_	start_char=38|end_char=40|ner=O
7	безусловен	безусловен	ADJ	Amsi	Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	9	amod	_	start_char=41|end_char=51|ner=O
8	основен	основен	ADJ	Amsi	Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	9	amod	_	start_char=52|end_char=59|ner=O
9	доход	доход	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	5	nmod	_	start_char=60|end_char=65|ner=O
10	в	в	ADP	R	_	11	case	_	start_char=66|end_char=67|ner=O
11	ЕС	ес	PROPN	Npmsi	Definite=Ind|Gender=Masc|Number=Sing	9	nmod	_	start_char=68|end_char=70|ner=S-ORG
12	.	.	PUNCT	punct	_	2	punct	_	start_char=70|end_char=71|ner=O


</comment_6965>
<comment_196353 alignment='In favor' langue='bg'>
# text = ❤️
# sent_id = 0
1	❤️	❤️	PUNCT	punct	_	0	root	_	start_char=0|end_char=2|ner=O


</comment_196353>
<comment_2198 alignment='Against' langue='bg'>
# text = Минималната заплата в България е 340 евро, а в Люксембург 2200 евро.
# sent_id = 0
1	Минималната	минимален	ADJ	Afsd	Definite=Def|Degree=Pos|Gender=Fem|Number=Sing	2	amod	_	start_char=0|end_char=11|ner=O
2	заплата	заплата	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	7	nsubj	_	start_char=12|end_char=19|ner=O
3	в	в	ADP	R	_	4	case	_	start_char=20|end_char=21|ner=O
4	България	българия	PROPN	Npfsi	Definite=Ind|Gender=Fem|Number=Sing	2	nmod	_	start_char=22|end_char=30|ner=S-LOC
5	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	7	cop	_	start_char=31|end_char=32|ner=O
6	340	340	NUM	Mc-pi	Definite=Ind|NumType=Card|Number=Plur	7	nummod	_	start_char=33|end_char=36|ner=O
7	евро	евро	NOUN	Ncnpi	Definite=Ind|Gender=Neut|Number=Plur	0	root	_	start_char=37|end_char=41|ner=O
8	,	,	PUNCT	punct	_	9	punct	_	start_char=41|end_char=42|ner=O
9	а	а	CCONJ	Cp	_	13	cc	_	start_char=43|end_char=44|ner=O
10	в	в	ADP	R	_	11	case	_	start_char=45|end_char=46|ner=O
11	Люксембург	люксембург	PROPN	Npmsi	Definite=Ind|Gender=Masc|Number=Sing	13	nmod	_	start_char=47|end_char=57|ner=S-LOC
12	2200	2200	NUM	Mc-pi	Definite=Ind|NumType=Card|Number=Plur	13	nummod	_	start_char=58|end_char=62|ner=O
13	евро	евро	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	7	conj	_	start_char=63|end_char=67|ner=O
14	.	.	PUNCT	punct	_	7	punct	_	start_char=67|end_char=68|ner=O

# text = Разликите в ЕС са прекалено големи за да е ефективна мярка минималната заплата за постигане на кавито и да било общоевропейски цели.
# sent_id = 1
1	Разликите	разлика	NOUN	Ncfpd	Definite=Def|Gender=Fem|Number=Plur	6	nsubj	_	start_char=69|end_char=78|ner=O
2	в	в	ADP	R	_	3	case	_	start_char=79|end_char=80|ner=O
3	ЕС	ес	PROPN	Npmsi	Definite=Ind|Gender=Masc|Number=Sing	1	nmod	_	start_char=81|end_char=83|ner=S-ORG
4	са	съм	AUX	Vxitf-r3p	Aspect=Imp|Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	6	cop	_	start_char=84|end_char=86|ner=O
5	прекалено	прекалено	ADV	Dq	Degree=Pos	6	advmod	_	start_char=87|end_char=96|ner=O
6	големи	голям	ADJ	A-pi	Definite=Ind|Degree=Pos|Number=Plur	0	root	_	start_char=97|end_char=103|ner=O
7	за	за	ADP	R	_	11	mark	_	start_char=104|end_char=106|ner=O
8	да	да	AUX	Tx	_	7	fixed	_	start_char=107|end_char=109|ner=O
9	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	11	cop	_	start_char=110|end_char=111|ner=O
10	ефективна	ефективен	ADJ	Afsi	Definite=Ind|Degree=Pos|Gender=Fem|Number=Sing	11	amod	_	start_char=112|end_char=121|ner=O
11	мярка	мярка	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	6	advcl	_	start_char=122|end_char=127|ner=O
12	минималната	минимален	ADJ	Afsd	Definite=Def|Degree=Pos|Gender=Fem|Number=Sing	13	amod	_	start_char=128|end_char=139|ner=O
13	заплата	заплата	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	11	nsubj	_	start_char=140|end_char=147|ner=O
14	за	за	ADP	R	_	15	case	_	start_char=148|end_char=150|ner=O
15	постигане	постигане	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	13	nmod	_	start_char=151|end_char=160|ner=O
16	на	на	ADP	R	_	17	case	_	start_char=161|end_char=163|ner=O
17	кавито	кави	NOUN	Ncnsd	Definite=Def|Gender=Neut|Number=Sing	15	nmod	_	start_char=164|end_char=170|ner=O
18	и	и	CCONJ	Cp	_	22	cc	_	start_char=171|end_char=172|ner=O
19	да	да	AUX	Tx	_	22	aux	_	start_char=173|end_char=175|ner=O
20	било	съм	AUX	Vxitcat-sni	Aspect=Imp|Definite=Ind|Gender=Neut|Mood=Ind|Number=Sing|VerbForm=Part|Voice=Act	22	cop	_	start_char=176|end_char=180|ner=O
21	общоевропейски	общоевропейски	ADJ	A-pi	Definite=Ind|Degree=Pos|Number=Plur	22	amod	_	start_char=181|end_char=195|ner=O
22	цели	цел	NOUN	Ncfpi	Definite=Ind|Gender=Fem|Number=Plur	11	conj	_	start_char=196|end_char=200|ner=O
23	.	.	PUNCT	punct	_	6	punct	_	start_char=200|end_char=201|ner=O

# text = Между другото, ефективността на минималната заплата по принцип е силно дебатирана.
# sent_id = 2
1	Между	между	ADP	R	_	2	case	_	start_char=203|end_char=208|ner=O
2	другото	друг	ADJ	Ansd	Definite=Def|Degree=Pos|Gender=Neut|Number=Sing	12	obl	_	start_char=209|end_char=216|ner=O
3	,	,	PUNCT	punct	_	2	punct	_	start_char=216|end_char=217|ner=O
4	ефективността	ефективност	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	12	nsubj	_	start_char=218|end_char=231|ner=O
5	на	на	ADP	R	_	7	case	_	start_char=232|end_char=234|ner=O
6	минималната	минимален	ADJ	Afsd	Definite=Def|Degree=Pos|Gender=Fem|Number=Sing	7	amod	_	start_char=235|end_char=246|ner=O
7	заплата	заплата	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	4	nmod	_	start_char=247|end_char=254|ner=O
8	по	по	ADP	R	_	9	case	_	start_char=255|end_char=257|ner=O
9	принцип	принцип	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	7	nmod	_	start_char=258|end_char=265|ner=O
10	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	12	cop	_	start_char=266|end_char=267|ner=O
11	силно	силно	ADV	Dm	Degree=Pos	12	advmod	_	start_char=268|end_char=273|ner=O
12	дебатирана	дебатирам	ADJ	Vpptcv--sfi	Aspect=Perf|Definite=Ind|Degree=Pos|Gender=Fem|Number=Sing|VerbForm=Part|Voice=Pass	0	root	_	start_char=274|end_char=284|ner=O
13	.	.	PUNCT	punct	_	12	punct	_	start_char=284|end_char=285|ner=O


</comment_2198>
<comment_186847 alignment='In favor' langue='bg'>
# text = Обожавам идеята Ви.
# sent_id = 0
1	Обожавам	обожавам	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=0|end_char=8|ner=O
2	идеята	идея	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	1	obj	_	start_char=9|end_char=15|ner=O
3	Ви	аз	PRON	Psht--2	Person=2|Poss=Yes|PronType=Prs	2	det	_	start_char=16|end_char=18|ner=O
4	.	.	PUNCT	punct	_	1	punct	_	start_char=18|end_char=19|ner=O

# text = Това би спомогнало и за здравето на децата.
# sent_id = 1
1	Това	този	PRON	Pde-os-n	Case=Nom|Gender=Neut|Number=Sing|PronType=Dem	3	nsubj	_	start_char=20|end_char=24|ner=O
2	би	съм	AUX	Vxitu-o3s	Aspect=Imp|Mood=Cnd|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	aux	_	start_char=25|end_char=27|ner=O
3	спомогнало	спомогна	VERB	Vpptcao-sni	Aspect=Perf|Definite=Ind|Gender=Neut|Number=Sing|Tense=Past|VerbForm=Part|Voice=Act	0	root	_	start_char=28|end_char=38|ner=O
4	и	и	CCONJ	Cp	_	6	cc	_	start_char=39|end_char=40|ner=O
5	за	за	ADP	R	_	6	case	_	start_char=41|end_char=43|ner=O
6	здравето	здраве	NOUN	Ncnsd	Definite=Def|Gender=Neut|Number=Sing	3	obj	_	start_char=44|end_char=52|ner=O
7	на	на	ADP	R	_	8	case	_	start_char=53|end_char=55|ner=O
8	децата	дете	NOUN	Ncnpd	Definite=Def|Gender=Neut|Number=Plur	6	nmod	_	start_char=56|end_char=62|ner=O
9	.	.	PUNCT	punct	_	3	punct	_	start_char=62|end_char=63|ner=O

# text = Би им дало ценно средство за преодоляване на тревожността, агресията, депресията.
# sent_id = 2
1	Би	съм	AUX	Vxitu-o3s	Aspect=Imp|Mood=Cnd|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	aux	_	start_char=64|end_char=66|ner=O
2	им	техен	PRON	Ppetdp3	Case=Dat|Number=Plur|Person=3|PronType=Prs	3	iobj	_	start_char=67|end_char=69|ner=O
3	дало	да	VERB	Vpptcao-sni	Aspect=Perf|Definite=Ind|Gender=Neut|Number=Sing|Tense=Past|VerbForm=Part|Voice=Act	0	root	_	start_char=70|end_char=74|ner=O
4	ценно	ценен	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	5	amod	_	start_char=75|end_char=80|ner=O
5	средство	средство	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	3	obj	_	start_char=81|end_char=89|ner=O
6	за	за	ADP	R	_	7	case	_	start_char=90|end_char=92|ner=O
7	преодоляване	преодоляване	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	5	nmod	_	start_char=93|end_char=105|ner=O
8	на	на	ADP	R	_	9	case	_	start_char=106|end_char=108|ner=O
9	тревожността	тревожност	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	7	nmod	_	start_char=109|end_char=121|ner=O
10	,	,	PUNCT	punct	_	11	punct	_	start_char=121|end_char=122|ner=O
11	агресията	агресия	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	9	conj	_	start_char=123|end_char=132|ner=O
12	,	,	PUNCT	punct	_	13	punct	_	start_char=132|end_char=133|ner=O
13	депресията	депресия	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	9	conj	_	start_char=134|end_char=144|ner=O
14	.	.	PUNCT	punct	_	3	punct	_	start_char=144|end_char=145|ner=O


</comment_186847>
<comment_2944 alignment='In favor' langue='bg'>
# text = Иначе е някак си дискриминиращо.
# sent_id = 0
1	Иначе	така	ADV	Pdm	PronType=Dem	5	advmod	_	start_char=0|end_char=5|ner=O
2	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	5	cop	_	start_char=6|end_char=7|ner=O
3	някак	някак	ADV	Pfm	PronType=Ind	5	advmod	_	start_char=8|end_char=13|ner=O
4	си	свой	PRON	Ppxtd	Case=Dat|PronType=Prs|Reflex=Yes	5	expl	_	start_char=14|end_char=16|ner=O
5	дискриминиращо	дискриминирам	ADJ	Vpitcar-sni	Aspect=Imp|Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing|Tense=Pres|VerbForm=Part|Voice=Act	0	root	_	start_char=17|end_char=31|ner=O
6	.	.	PUNCT	punct	_	5	punct	_	start_char=31|end_char=32|ner=O


</comment_2944>
<comment_186781 alignment='In favor' langue='bg'>
# text = Напълно подкрепям идеята.
# sent_id = 0
1	Напълно	напълно	ADV	Dq	Degree=Pos	2	advmod	_	start_char=0|end_char=7|ner=O
2	подкрепям	подкрепям	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=8|end_char=17|ner=O
3	идеята	идея	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	2	obj	_	start_char=18|end_char=24|ner=O
4	.	.	PUNCT	punct	_	2	punct	_	start_char=24|end_char=25|ner=O

# text = В различните курсове, форуми, групи, посветени на ЕС, в които участвам, винаги се стига до идеята за телевизия, за медия.
# sent_id = 1
1	В	в	ADP	R	_	3	case	_	start_char=26|end_char=27|ner=O
2	различните	различен	ADJ	A-pd	Definite=Def|Degree=Pos|Number=Plur	3	amod	_	start_char=28|end_char=38|ner=O
3	курсове	курс	NOUN	Ncmpi	Definite=Ind|Gender=Masc|Number=Plur	19	obl	_	start_char=39|end_char=46|ner=O
4	,	,	PUNCT	punct	_	5	punct	_	start_char=46|end_char=47|ner=O
5	форуми	форум	NOUN	Ncmpi	Definite=Ind|Gender=Masc|Number=Plur	3	conj	_	start_char=48|end_char=54|ner=O
6	,	,	PUNCT	punct	_	7	punct	_	start_char=54|end_char=55|ner=O
7	групи	група	NOUN	Ncfpi	Definite=Ind|Gender=Fem|Number=Plur	3	conj	_	start_char=56|end_char=61|ner=O
8	,	,	PUNCT	punct	_	9	punct	_	start_char=61|end_char=62|ner=O
9	посветени	посветя-(се)	ADJ	Vpptcv--p-i	Aspect=Perf|Definite=Ind|Degree=Pos|Number=Plur|VerbForm=Part|Voice=Pass	3	amod	_	start_char=63|end_char=72|ner=O
10	на	на	ADP	R	_	11	case	_	start_char=73|end_char=75|ner=O
11	ЕС	ес	PROPN	Npmsi	Definite=Ind|Gender=Masc|Number=Sing	9	obl	_	start_char=76|end_char=78|ner=S-ORG
12	,	,	PUNCT	punct	_	15	punct	_	start_char=78|end_char=79|ner=O
13	в	в	ADP	R	_	14	case	_	start_char=80|end_char=81|ner=O
14	които	който	PRON	Pre-op	Case=Nom|Number=Plur|PronType=Rel	15	iobj	_	start_char=82|end_char=87|ner=O
15	участвам	участвам	VERB	Vpiif-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	11	acl:relcl	_	start_char=88|end_char=96|ner=O
16	,	,	PUNCT	punct	_	15	punct	_	start_char=96|end_char=97|ner=O
17	винаги	винаги	ADV	Dt	Degree=Pos	19	advmod	_	start_char=98|end_char=104|ner=O
18	се	се	PRON	Ppxta	Case=Acc|PronType=Prs|Reflex=Yes	19	expl	_	start_char=105|end_char=107|ner=O
19	стига	стигам	VERB	Vpitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=108|end_char=113|ner=O
20	до	до	ADP	R	_	21	case	_	start_char=114|end_char=116|ner=O
21	идеята	идея	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	19	iobj	_	start_char=117|end_char=123|ner=O
22	за	за	ADP	R	_	23	case	_	start_char=124|end_char=126|ner=O
23	телевизия	телевизия	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	21	nmod	_	start_char=127|end_char=136|ner=O
24	,	,	PUNCT	punct	_	26	punct	_	start_char=136|end_char=137|ner=O
25	за	за	ADP	R	_	26	case	_	start_char=138|end_char=140|ner=O
26	медия	медия	NOUN	Ncfsi	Definite=Ind|Gender=Fem|Number=Sing	23	conj	_	start_char=141|end_char=146|ner=O
27	.	.	PUNCT	punct	_	19	punct	_	start_char=146|end_char=147|ner=O

# text = Това би помогнало, както за културата, така и за демокрацията.
# sent_id = 2
1	Това	този	PRON	Pde-os-n	Case=Nom|Gender=Neut|Number=Sing|PronType=Dem	3	nsubj	_	start_char=148|end_char=152|ner=O
2	би	съм	AUX	Vxitu-o3s	Aspect=Imp|Mood=Cnd|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	aux	_	start_char=153|end_char=155|ner=O
3	помогнало	помогна	VERB	Vppicao-sni	Aspect=Perf|Definite=Ind|Gender=Neut|Number=Sing|Tense=Past|VerbForm=Part|Voice=Act	0	root	_	start_char=156|end_char=165|ner=O
4	,	,	PUNCT	punct	_	7	punct	_	start_char=165|end_char=166|ner=O
5	както	както	ADV	Prm	PronType=Rel	7	cc	_	start_char=167|end_char=172|ner=O
6	за	за	ADP	R	_	7	case	_	start_char=173|end_char=175|ner=O
7	културата	култура	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	3	obl	_	start_char=176|end_char=185|ner=O
8	,	,	PUNCT	punct	_	9	punct	_	start_char=185|end_char=186|ner=O
9	така	така	ADV	Pdm	PronType=Dem	12	cc	_	start_char=187|end_char=191|ner=O
10	и	и	CCONJ	Cp	_	9	fixed	_	start_char=192|end_char=193|ner=O
11	за	за	ADP	R	_	12	case	_	start_char=194|end_char=196|ner=O
12	демокрацията	демокрация	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	7	conj	_	start_char=197|end_char=209|ner=O
13	.	.	PUNCT	punct	_	3	punct	_	start_char=209|end_char=210|ner=O


</comment_186781>
<comment_132665 alignment='In favor' langue='bg'>
# text = В спешни ситуации владеенето на език може да е животоспасяващо.
# sent_id = 0
1	В	в	ADP	R	_	3	case	_	start_char=0|end_char=1|ner=O
2	спешни	спешен	ADJ	A-pi	Definite=Ind|Degree=Pos|Number=Plur	3	amod	_	start_char=2|end_char=8|ner=O
3	ситуации	ситуация	NOUN	Ncfpi	Definite=Ind|Gender=Fem|Number=Plur	7	obl	_	start_char=9|end_char=17|ner=O
4	владеенето	владеене	NOUN	Ncnsd	Definite=Def|Gender=Neut|Number=Sing	7	nsubj	_	start_char=18|end_char=28|ner=O
5	на	на	ADP	R	_	6	case	_	start_char=29|end_char=31|ner=O
6	език	език	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	4	nmod	_	start_char=32|end_char=36|ner=O
7	може	мога	VERB	Vpiif-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=37|end_char=41|ner=O
8	да	да	AUX	Tx	_	10	aux	_	start_char=42|end_char=44|ner=O
9	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	10	cop	_	start_char=45|end_char=46|ner=O
10	животоспасяващо	животоспасявам-(се)	ADJ	Ansi	Definite=Ind|Degree=Pos|Gender=Neut|Number=Sing	7	xcomp	_	start_char=47|end_char=62|ner=O
11	.	.	PUNCT	punct	_	7	punct	_	start_char=62|end_char=63|ner=O

# text = Но не е възможно всички хора да знаят всички езици.
# sent_id = 1
1	Но	но	CCONJ	Cc	_	4	cc	_	start_char=64|end_char=66|ner=O
2	не	не	PART	Tn	Polarity=Neg	4	advmod	_	start_char=67|end_char=69|ner=O
3	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	4	cop	_	start_char=70|end_char=71|ner=O
4	възможно	възможно	ADV	Dd	Degree=Pos	0	root	_	start_char=72|end_char=80|ner=O
5	всички	всеки	DET	Pce-op	Case=Nom|Number=Plur|PronType=Tot	6	det	_	start_char=81|end_char=87|ner=O
6	хора	хора	NOUN	Nc-li	Definite=Ind|Number=Ptan	8	nsubj	_	start_char=88|end_char=92|ner=O
7	да	да	AUX	Tx	_	8	aux	_	start_char=93|end_char=95|ner=O
8	знаят	знам	VERB	Vpiif-r3p	Aspect=Imp|Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	4	csubj	_	start_char=96|end_char=101|ner=O
9	всички	всеки	DET	Pce-op	Case=Nom|Number=Plur|PronType=Tot	10	det	_	start_char=102|end_char=108|ner=O
10	езици	език	NOUN	Ncmpi	Definite=Ind|Gender=Masc|Number=Plur	8	obj	_	start_char=109|end_char=114|ner=O
11	.	.	PUNCT	punct	_	4	punct	_	start_char=114|end_char=115|ner=O

# text = Затова пък е възможно всички да ползват един общ допълнителен език - есперанто.
# sent_id = 2
1	Затова	затова	ADV	Pds	_	4	advmod	_	start_char=116|end_char=122|ner=O
2	пък	пък	PART	Te	_	4	discourse	_	start_char=123|end_char=126|ner=O
3	е	съм	AUX	Vxitf-r3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	4	cop	_	start_char=127|end_char=128|ner=O
4	възможно	възможно	ADV	Dd	Degree=Pos	0	root	_	start_char=129|end_char=137|ner=O
5	всички	всеки	PRON	Pce-op	Case=Nom|Number=Plur|PronType=Tot	7	nsubj	_	start_char=138|end_char=144|ner=O
6	да	да	AUX	Tx	_	7	aux	_	start_char=145|end_char=147|ner=O
7	ползват	ползвам-(се)	VERB	Vpitf-r3p	Aspect=Imp|Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin|Voice=Act	4	csubj	_	start_char=148|end_char=155|ner=O
8	един	един	DET	Pfe-os-mi	Case=Nom|Definite=Ind|Gender=Masc|Number=Sing|PronType=Ind	11	det	_	start_char=156|end_char=160|ner=O
9	общ	общ	ADJ	Amsi	Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	11	amod	_	start_char=161|end_char=164|ner=O
10	допълнителен	допълнителен	ADJ	Amsi	Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	11	amod	_	start_char=165|end_char=177|ner=O
11	език	език	NOUN	Ncmsi	Definite=Ind|Gender=Masc|Number=Sing	7	obj	_	start_char=178|end_char=182|ner=O
12	-	-	PUNCT	punct	_	13	punct	_	start_char=183|end_char=184|ner=O
13	есперанто	есперант	NOUN	Ncnsi	Definite=Ind|Gender=Neut|Number=Sing	11	nmod	_	start_char=185|end_char=194|ner=O
14	.	.	PUNCT	punct	_	4	punct	_	start_char=194|end_char=195|ner=O


</comment_132665>
<comment_130783 alignment='In favor' langue='bg'>
# text = Подкрепям идеята.
# sent_id = 0
1	Подкрепям	подкрепям	VERB	Vpitf-r1s	Aspect=Imp|Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin|Voice=Act	0	root	_	start_char=0|end_char=9|ner=O
2	идеята	идея	NOUN	Ncfsd	Definite=Def|Gender=Fem|Number=Sing	1	obj	_	start_char=10|end_char=16|ner=O
3	.	.	PUNCT	punct	_	1	punct	_	start_char=16|end_char=17|ner=O


</comment_130783>
