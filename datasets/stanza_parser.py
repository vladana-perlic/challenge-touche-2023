import pandas as pd
import sys
# pip install stanza
from wrapper_stanza import parser
from data_by_lang import *

try:
    data = sys.argv[1]
    st_lang = sys.argv[2]
    data_filtered_by_langue = filter_data_by_language(data, st_lang)
    parser(text=data_filtered_by_langue[0], lang=st_lang, out=data_filtered_by_langue[1])
except:
    print("You maybe include the data an the language as parameters")

