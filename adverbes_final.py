import sys
import re


adv_neg_against = 0
adv_neg_in_favor = 0
adv_neg_against_fr=0
adv_neg_against_es=0
adv_neg_against_en=0
adv_neg_in_favor_fr = 0
adv_neg_in_favor_es = 0
adv_neg_in_favor_en = 0

adv_pos_against = 0
adv_pos_in_favor = 0
adv_pos_against_fr=0
adv_pos_against_es=0
adv_pos_against_en=0
adv_pos_in_favor_fr = 0
adv_pos_in_favor_es = 0
adv_pos_in_favor_en = 0
com = 0
many_adv=0


for line in sys.stdin:
    line = line.rstrip("\n")

    l = line.split("\t")

    if l[0].startswith('<comment_'):    # Cmmentaire qui répond directement à la proposition
        com = 1
        match = re.search(r"<comment_(\d{5}) alignment=\'(In favor|Against)\' langue=\'(..)\'>", l[0])
        if match:
            numero = match.group(1)
            opinion = match.group(2)
            langue = match.group(3)
    elif l[0].startswith('</comment_'):
        com = 0
        many_adv = 0

         
    if opinion == "Against":
        if len(l) == 10:
            if l[5] == "Polarity=Neg" and many_adv==0:
                many_neg=1
                adv_neg_against=adv_neg_against+1
                if langue == "es":
                    adv_neg_against_es=adv_neg_against_es+1
                if langue == "en":
                    adv_neg_against_en=adv_neg_against_en+1
                if langue == "fr":
                    adv_neg_against_fr=adv_neg_against_fr+1
            elif l[5] == "Polarity=Pos" and many_neg==0:
                many_adv = 0
                adv_pos_against=adv_pos_against+1
                if langue == "es":
                    adv_pos_against_es=adv_pos_against_es+1
                if langue == "en":
                    adv_pos_against_en=adv_pos_against_en+1
                if langue == "fr":
                    adv_pos_against_fr=adv_pos_against_fr+1
            
    elif opinion == "In favor":
        if len(l) == 10:
            if l[5] == "Polarity=Neg" and many_adv==0:
                many_adv = 1
                adv_neg_in_favor=adv_neg_in_favor+1
                if langue == "es":
                    adv_neg_in_favor_es=adv_neg_in_favor_es+1
                if langue == "en":
                    adv_neg_in_favor_en=adv_neg_in_favor_en+1
                if langue == "fr":
                    adv_neg_in_favor_fr=adv_neg_in_favor_fr+1
            elif l[5] == "Polarity=Pos" and many_adv==0:
                many_adv = 1
                adv_pos_in_favor=adv_pos_in_favor+1
                if langue == "es":
                    adv_pos_in_favor_es=adv_pos_in_favor_es+1
                if langue == "en":
                    adv_pos_in_favor_en=adv_pos_in_favor_en+1
                if langue == "fr":
                    adv_pos_in_favor_fr=adv_pos_in_favor_fr+1
print(f"Against adv neg total: {adv_neg_against}")
print(f"Against adv neg espagnol: {adv_neg_against_es}")
print(f"Against adv neg anglais: {adv_neg_against_en}")
print(f"Against adv neg français: {adv_neg_against_fr}")
print(f"In favor adv neg total: {adv_neg_in_favor}")
print(f"In favor adv neg espagnol: {adv_neg_in_favor_es}")
print(f"In favor adv neg anglais: {adv_neg_in_favor_en}")
print(f"In favor adv neg français: {adv_neg_in_favor_fr}")

print(f"Against adv pos total: {adv_pos_against}")
print(f"Against adv pos espagnol: {adv_pos_against_es}")
print(f"Against adv pos anglais: {adv_pos_against_en}")
print(f"Against adv pos français: {adv_pos_against_fr}")
print(f"In favor adv pos total: {adv_pos_in_favor}")
print(f"In favor adv pos espagnol: {adv_pos_in_favor_es}")
print(f"In favor adv pos anglais: {adv_pos_in_favor_en}")
print(f"In favor adv pos français: {adv_pos_in_favor_fr}")