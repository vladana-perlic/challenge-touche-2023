import numpy as np


def proba_infavor(tokens: list[list]) -> float:
    """
    Cette fonction utilise la régression polynomiale de 3e degré, sur la base des observations de la répartition de commentaires infavor et against dans les jeux de données CFs et CFe-dev sur toutes les langues, pour donner une probabilité d'appartenance du commentaire à la classe infavor en fonction de sa taille en tokens.
    En entrée : Une liste de tokens, dont chaque item correspond à une liste d'informations sur un token
    En sortie : Un nombre réel compris entre 0 et 1, correspondant à la probabilité qu'à ce commentaire d'être classé infavor
    """
    # Données fournies
    x = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230]
    y_rel = [73.548883099133, 56.8660079382789, 57.1538850877736, 54.834154897162, 49.1027349135262, 43.0614411206673, 39.0966628956304, 45.2128080606786, 44.7618851587318, 42.9724669265478, 39.8282194283049, 43.0030831530993, 43.202183846656, 42.9920650993243, 43.4878961296294, 40.5025477233631, 48.7037190424915, 42.5233920842368, 33.6156072462215, 35.1721972155344, 39.1829519787882, 11.9436940139343, 21.3387526990982]
    y_abs = [91.1111111111111, 82.9347826086957, 83.1003811944092, 81.7365269461078, 78.0525502318393, 73.6, 70.2947845804989, 75.2604166666667, 74.91961414791, 73.5294117647059, 70.9302325581395, 73.5537190082645, 73.7113402061856, 73.5449735449735, 73.936170212766, 71.505376344086, 77.7777777777778, 73.1707317073171, 65.1162790697674, 66.6666666666667, 70.3703703703704, 33.3333333333333, 50]

    # Régression polynomiale de degré 3
    coef = np.polyfit(x, y_rel, 3)
    
    return np.polyval(coef, len(tokens))/100
    #return sum([coef[i]*(n**(len(coef)-i-1)) for i in range(len(coef))])/100

def proba_against(tokens: list[list]) -> float:
    """
    Cette fonction se base sur la fonction proba_infavor pour donner une probabilité d'appartenance du commentaire à la classe against en fonction de sa taille en tokens.
    En entrée : Une liste de tokens, dont chaque item correspond à une liste d'informations sur un token
    En sortie : Un nombre réel compris entre 0 et 1, correspondant à la probabilité qu'à ce commentaire d'être classé against
    """
    return 1-proba_infavor(tokens)
