import numpy as np
import sys
import re
import csv

def proba_infavor(tokens: list[list]) -> float:
    """
    Cette fonction utilise la régression polynomiale de 3e degré, sur la base des observations de la répartition de commentaires infavor et against dans les jeux de données CFs et CFe-dev sur toutes les langues, pour donner une probabilité d'appartenance du commentaire à la classe infavor en fonction de sa taille en tokens.
    En entrée : Une liste de tokens, dont chaque item correspond à une liste d'informations sur un token
    En sortie : Un nombre réel compris entre 0 et 1, correspondant à la probabilité qu'à ce commentaire d'être classé infavor
    Shaad
    """
    # Données fournies
    x = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230]
    y_rel = [73.548883099133, 56.8660079382789, 57.1538850877736, 54.834154897162, 49.1027349135262, 43.0614411206673, 39.0966628956304, 45.2128080606786, 44.7618851587318, 42.9724669265478, 39.8282194283049, 43.0030831530993, 43.202183846656, 42.9920650993243, 43.4878961296294, 40.5025477233631, 48.7037190424915, 42.5233920842368, 33.6156072462215, 35.1721972155344, 39.1829519787882, 11.9436940139343, 21.3387526990982]
    y_abs = [91.1111111111111, 82.9347826086957, 83.1003811944092, 81.7365269461078, 78.0525502318393, 73.6, 70.2947845804989, 75.2604166666667, 74.91961414791, 73.5294117647059, 70.9302325581395, 73.5537190082645, 73.7113402061856, 73.5449735449735, 73.936170212766, 71.505376344086, 77.7777777777778, 73.1707317073171, 65.1162790697674, 66.6666666666667, 70.3703703703704, 33.3333333333333, 50]

    # Régression polynomiale de degré 3
    coef = np.polyfit(x, y_abs, 3)
    
    return np.polyval(coef, len(tokens))/100
    #return sum([coef[i]*(n**(len(coef)-i-1)) for i in range(len(coef))])/100

def proba_against(tokens: list[list]) -> float:
    """
    Cette fonction se base sur la fonction proba_infavor pour donner une probabilité d'appartenance du commentaire à la classe against en fonction de sa taille en tokens.
    En entrée : Une liste de tokens, dont chaque item correspond à une liste d'informations sur un token
    En sortie : Un nombre réel compris entre 0 et 1, correspondant à la probabilité qu'à ce commentaire d'être classé against
    Shaad
    """
    return 1-proba_infavor(tokens)

def ponctuation_score(text: str) -> int:
    """
    Cette fonction donne un score basé sur le nombre de ! et ? dans la chaîne en entrée
    En entrée : Une chaîne de caractères
    En sortie : Un nombre entier, correspondant à la différence entre les points d'exclamation et les points d'interrogation présents dans la chaîne
    Vladana
    """
    num_excl = text.count("!")
    num_int = text.count("?")
    return num_excl - num_int

def nombre_de_neg(tokens: list[list]) -> tuple[float]:
    """
    Cette fonction donne le nombre de tokens marqués Polarity=Neg dans le commentaire
    En entrée : Une liste de tokens, dont chaque item correspond à une liste d'informations sur un token
    En sortie : Un tuple de nombres entiers, correspondant au nombre de tokens negatifs présents dans la chaîne, et au nombre d'adverbes négatifs
    Charlotte et Anca
    """
    n_neg = 0
    n_adv_neg = 0
    for t in tokens:
        if "Polarity=Neg" in t[5]:
            n_neg += 1
            if t[3] == "ADV":
                n_adv_neg += 1
    return n_neg, n_adv_neg

def score_expression(commentaire: str, langue: str):
    """
    Cette fonction donne un score se basant sur la différence du nombre d'expressions explicites d'accord et de désaccord dans chaque commentaire
    En entrée : Un commentaire en chaîne de caractères, suivi de la langue du commentaire
    En sortie : Un nombre entier, correspondant à la différence entre les expressions pour et les expressions contre présentes dans la chaîne
    Angélo
    """
    # Dictionnaire de listes d'expressions regex
    listes = {
         "fr" : {
            "In favor" : [r"(j|J)e\ssuis\s(.+)?favorable", r"(j|J)e\ssuis\s(.+)?d'accord", r"(j|J)e\ssuis\s(.+)?pour"],
            "Against" : [r"(j|J)e\ssuis\s(.+)?opposée?", r"(j|J)e\ssuis\s(.+)?contre", r"(j|J)e(\sne)?\ssuis\spas\s(.+)?favorable", r"(j|J)e(\sne)?\ssuis\spas\s(.+)?d'accord", r"(j|J)e(\sne)?\ssuis\spas\s(.+)?pour"]
         },
         "da" : {
            "In favor" : [r"(j|J)eg\s(er|går)\s(ind\s)?(enig|for)\s", r"aftalt", r"helt\senig\s"],
            "Against" : [r"(j|J)eg\s(er|går)\s(imod|ikke)\s(enig|ind)?(\sfor)?", r"\b(j|J)eg\s afviser\s"]
         },
         "hu" : {
            "In favor" : [r"(en)?\stámogatom\s", r"(teljesen)?\segyetértek\s(vele)?\s"],
            "Against" : [r"(en)?\sellenzem\sa?\s", r"nem\sertek(\svele)?(\sezzel)?\segyet\s", r"(elutasitom|visszautasítom)"]
         },
         "cs" : {
            "In favor" : [r"(já)?\sjesem\spro\s", r"(naprosto|naprostý|zcela)?\bsouhlas(ím)?\s", r"souhlas(í|il)|dohodnuto"],
            "Against" : [r"jsem\sproti(\stomu)?\s", r"nesouhlasim"]
         },
         "sk" : {
            "In favor" : [r"(s|S)úhlasím\s", r"(ja\s)?som\sza\s", r"celkom|úplne|dost"],
            "Against" : [r"(n|N)esúhlasím\s(s\s)?(tým\s)?", r"(s|S)om\sproti\s(tomu\s)?"]
         },
         "sl" : {
            "In favor" : [r"(s|S)trinjam\sse\s", r"(P|p)odpiram", r"(S|s)em\sza\s", r"dogovorjeno"],
            "Against" : [r"(N|n)asprotujem", r"(s|S)em\sproti\s", r"(se\s)?(N|n)e\sstrinjam\s(se\s)?"]
         },
         "en" : {
            "In favor" : [ r'\bYes\b',r'\bAgreed\b',r'\bAbsolutely\b',r'\bDefinitely\b',r'\bTrue\b',r'\bGood idea\b',r'\bGreat\b'],
            "Against" : [r'\bNo\b',r'\bNot\b',r'\bBad idea\b',r"\bDon't agree\b",r'\bI disagree\b']
         },
         "pl" : {
            "In favor" : [r"[zZ]gadzam\ssi[ęe]\sz\stym", r"[dD]obry\spomys[łl]", r"[wW]spaniał[ae]\s.*inicjatyw[ae]", r"[Rr]ozsądne\srozwiązanie", r"^(?!.*nie)[jJ]estem\szadowolon[ya]"],
            "Against" : [r"([tT]o)?\snie\sjest\sdobry\spomys[łl]", r"([Tt]en)?\spomys[łl]\sjest\sz[łl]y", r"[tT]o.*\snie\sjest\sdobra\sinicjatywa", r"[tT]o\srozwi[ąa]zanie\snie\sjest\srozs[ąa]dne", r"[nN]ie\sjestem\szadowolon[ya]"]
         },
         "nl" : {
            "In favor" : [r"(helemaal\s)?(?<!niet\s)((mee\s)?(?<!niet mee\s)eens)", r"(?<!niet\s)(heel\s)?(?<!niet heel\s)(?<!niet een\s)(?<!niet een heel\s)goed idee"],
            "Against" : [r"niet\s(\w+\s)?akkoord", r"(?<!niet\s)tegen", r"(?<!niet\s)(heel\s)?(?<!niet heel\s)(?<!niet een\s)(?<!niet een heel\s)slecht idee", r"(?<!niet\s)akkoord"]
         },
         "it" : {
            "In favor" : [r"(?:(S|s)ono\sassolutamente\s+|(A|a)ssolutamente\s+|(S|s)ono\s+)\b(D|d)'accordo|(F|f)avorevole\b",r"(?:(S|s)ono\s+totalmente\s|(T|t)otalmente\s|(S|s)ono\s+)?a\s+favore",r"\b((B|b)ellissime|(E|e)ccellente|(B|b)uona|(O|o)ttima|(S|s)plendida)\b\s\b(idea|proposta)\b",r"b^(?!non\s+)(S|s)ottoscrivo\b",r"\b^(?!non\s+)(C|c)ondivido\b",r"\b^(?!non\s+)(A|a)ppoggio\b",r"\b^(?!non\s+)(A|a)pprovo\b"],
            "Against" : [r"\b(I|i)dea\s(?: è)?(stupida|pessima|cattiva)\b",r"\b(no|NO|No)\b",r"\b(S|s)ono\b\s\bcontro\b",r"\b(N|n)on\b\ssono\s(d’accord|favorevole|a\sfavore)"]
         },
         "bg" : {
            "In favor" : [r"\b(П|п)одкрепям/(П|п)оддържам\b\s\b(коментара/идеята)\b",r"\b(О|o)божавам]\s\bидеята\b",r"\b(С|с)ъглас(ен|на)\b\s\bсъм\b"],
            "Against" : [r"\b(Н|н)е\b\s\b(подкрепям/поддържам)\b",r"\b(А|а)з\b\s\bсъм\b\sпротив\b",r"\b(Л|л)оша\b\s\b(идея|идеята)\b",r"\b(Н|н)е\b\s\bсъм\b(\S*)\s\bсъглас(ен|на)\b"]
         },
         "eo" : {
            "In favor" : [r"\b(M|m)i\b\s\bkonsentas\b",r"\b(M|m)i\b\s\bne\b\s\bestas\b\s\bkontrau\b",r"\b(T|t)ute\b\sguste\b",r"\b((B|b)onega|(B|b)ona)\b\s\bideo\b"],
            "Against" : [r"\b(M|m)alguste\b",r"\b(M|m)albona\b\s\bideo\b",r"\bNe\b",r"\b(M|m)i\b\s\bmalkonsentas\b",r"\b(M|m)i\b\s\bne\b\s\b subtenas\b"]
         },
         "pt" : {
            "In favor" : [r"\b(C|c)oncordo\b",r"\b(S|s)ubscrevo\b",r"\b(A|a)poio\b",r"\b(E|e)xcelente\b\s\b ideia\b",r"\b(G|g)ostaria\b\s\bde\b\s\bapoiar\b"],
            "Against" : [r"\b(M|m)á|(P|p)éssima)\b\s\bideia\b"]
         },
         "et" : {
            "In favor" : [r"\b(T|t)oetame\b",r"\b(O|o)len\b\s\bnõus\b",r"\b((S|s)uurepärane|(H|h)ea)\b\s\bidee\b",r"((M|m)a\s)?\bnõustun\b"],
            "Against" : [r"\b(M|m)a\b\s\bolen\b\s\bvastu\b",r"\b(M|m)a\b\s\bei\b\s\btoeta\b",r"\b(M|m)a\b\s\bei\b\s\bole\b\s\bnõus\b",r"\b(H|h)alb\b\s\b((M|m)õte|(I|i)dee)\b"]
         },
         "fi" : {
            "In favor" : [r"\b(K|k)yllä\b",r"\b(J|k)ättebra\b",r"\b(K|k)annatan\b",r"\b((H|h)yvä|(H|h)ieno)\b\s\b((I|i)dea|(E|e)hdotus)\b",r"\b((T|t)äysin|(O|o)len)\b\s\bsamaa\b\s\bmieltä\b"],
            "Against" : [r"\bEn\b\sole\b\s(täysin)\b\ssamaa\b\smieltä\b",r"(\bMinä)?\b\sEn\s(kannata)|(tue)",r"(\bMinä)?\b\sVastustan\b",r"(\bhuono)|(\bkauhea)\s(\bidea)|(\bajatus)\b"]
         },
         "sv" : {
            "In favor" : [r"((M|m)ycket|(V|v)äldigt)?/\s\bbra\b(\s\bförslag\b)?",r"\b(E|e)tt\b\s\b((J|j)ättebra(N|n)ödvändigt(V|v)iktigt)\b\s\bförslag\b",r"\b(H|h)åller\b\s\bmed\b",r"\b(A|a)bsolut\b"],
            "Against" : [r"\b(J|j)ag\b\s\bstöder\b\s\binte\b"]
         },
         "hr" : {
            "In favor" : [r"slažem se", r"saglas(an|na)\s+sam", r"u potpunosti se slažem", r"podržavam", r"puna podrška", r"vašeg sam mišljenja", r"dijelim(\s+vaše)?\s+mišljenje", r"ideja\s+je\s+(sjajna|izvrsna|dobra|odlična)"],
            "Against" : [r"ne slažem\s+se", r"protivim\s+se", r"ne podržavam", r"nisam\s+saglas(an|na)", r"u potpunosti\s+sam\s+protiv", r"nisam\s+vašeg\s+mišljenja", r"ne dijelim(\s+vaše)?\s+mišljenje", r"ideja\s+je\s+(loša|grozna|užasna|glupa)"]
         },
         "es" : {
            "In favor" : [r"\bTotalmente\b\de\b\sacuerdo\b",r"\bNo\b\spodría\b\sestar\b\smás\b\sde\b\sacuerdo\b\s",r"\bTienes\b\sla\b\sboca\b\sllena\b\sde\b\sverdad\b"],
            "Against" : [r"\bClaro\b\sque\b\sno\b",r"\bEn\b\sabsoluto\b",r"\bNo\b\ses\b\sposible\b\spensar\b\seso\b\sasí\b"]
         },
         "el" : {
            "In favor" : [r"\bΔεν\b\sθα\b\sμπορούσα\b\sνα\b\sσυμφωνήσω\s\bπερισσότερο",r"\bαπολύτως\s\bσύμφωνος\b",r"\bαπόλυτα\b",r"\bσύμφωνοι\b",r"συμφωνώ\b"],
            "Against" : [r"\bΔεν\b\sσυμφωνώ\b\sκαθόλου\b",r"\bδιαφωνία\b",r"\bδιαφωνίες\b"]
         },
         "ca" : {
            "In favor" : [r"(j|J)o\s(estic|suporto)\s(d’acord)?", r"estic\sa\sfavor\sde"],
            "Against" : [r"(j|J)o\s(no\sestic\sd'acord\s(amb)?|rebutjo)\s", r"estic\sen\scontra\sde"]
         },
         "lv" : {
            "In favor" : [r"\bEs\b\snevarētu\b\svairāk\b\spiekrist\b",r"\bEs\b\snevaru\b\spiekrist\b\svairāk\b",r"\bNevarētu\b\spiekrist\b\svairāk\b",r"\bpilnīgi\b\spiekrītam\b",r"\bpilnīgi\b\spiekrīt\b",r"\bpilnīgi\b\spiekrītu\b"],
            "Against" : [r"\bEs\b\stam\b\snepavisam\b\snepiekrītu\b",r"\bEs\b\snepavisam\s\bnepiekrītu\b",r"\bEs\b\svispār\b\sepiekrītu\b\s",r"\bnesaskaņas\b",r"\bdomstarpības\b",r"\bEs\b\snepiekrītu\b"]
         },
         "lt" : {
            "In favor" : [r"\bNegalėčiau\b\ssu\b\stuo\b\ssutikti\b",r"\bNegalėčiau\b\slabiau\b\ssutikti\b",r"\bvisiškai\b\ssutinku\b",r"\bvisiškai\b\ssu\b\stuo\b\ssutinku\b"],
            "Against" : [r"\bAš\b\svisiškai\b",r"\bVisai\b\snesutinku\b",r"\bAš\bvisai\b\snesutinku\b",r"\bVisiškai\b",r"\bnesutarimai\b",r"\bnesutarima\b"]
         },
         "de" : {
            "In favor" : [r"\b(E|e)inverstanden\b", r"\b(G)|genauso\b\s\bist\b\s\b(es|das)\b", r"\bGenauso\sist\s(es|das)\b", r"\bGründzatzlich\sdafür\b", r"\bDa\sGebe\sIch\sDir\sRecht\b", r"\bEine\sgute\sIdee\b", r"\bEin\ssehr\sguter\sVorschlag\b", r"\bIch\sstimme\sabsolut\szu\b", r"\bKann\sman\snur\sunterstützen\b", r"\bIch\sunterstütze\sdiesen\sVorschlag\b", r"\bIch\sunterstütze\sdie\sAussage\b", r"\bDa\sMuss\sIch\sIhnen\sRecht\sGeben\b", r"\bIch\sbin\svoll\sund\sganz\sauch\sdieser\sMeinung\b", r"\bVolle\sZustimmung\b", r"\bDafür\b", r"\bDas\shalte\sich\sfür\seine\sgute\sGrundidee\b"],
            "Against" : [r"\bDas\shalte\sich\sehrlich\sgesagt\sfür\skeine\sgute\sIdee\b", r"\bIch\sglaube\snicht,\sdass\sdas\seine\sgute\sIdee\sist\b", r"\bIch\sbin\smit\sdiesem\sVorschlag\snicht\seinverstanden\b", r"\bVöllig\sfalsch\b", r"\bIch\sbin\sdagegen\b", r"\bIch\sbin\sgegen\sdiesen\sVorschlag\b", r"\bIch\skann\sdiesen\sVorschlag\snicht\sunterstützen\b"]
         },
         "ro" : {
            "In favor" : [r"\bsunt\b\s\bpentru\b", r"\bTotal\sde\sacord\b", r"\bSunt\s(complet|total)\sde \saccord\b", r"\bComplet\sde\saccord\b"],
            "Against" : [r"\bsunt\b\s\b(contra|împotriva)\b", r"\bNu\ssunt\sde\saccord\b", r"\bNu\ssunt\sdeloc\sde\saccord\b", r"\bNu\smi\sse\spare\so\sidee \sbună\b", r"\bBineînțeles\scă\snu\b"]
         }
    }

    score = 0
    if langue in listes:
        for regex in listes[langue]["In favor"]:
            match = re.search(regex, commentaire, flags=re.IGNORECASE)
            if match:
                print(match)
                score += 1
        for regex in listes[langue]["Against"]:
            match = re.search(regex, commentaire, flags=re.IGNORECASE)
            if match:
                print(match)
                score -= 1    
    return score

def prediction(longueur_infavor, ponctuation, negation, adverbes_negatifs, expressions):
    pred = longueur_infavor*0.5609 + ponctuation*0.1340 + negation*-0.6350 + adverbes_negatifs*0.5854 + expressions*0.3144 + 0.1427
    if pred <= 0.5:
        return "Against"
    else:
        return "In favor"
    #y_rel
    # pred = longueur_infavor*0.3869 + ponctuation*0.1342 + negation*-0.6355 + adverbes_negatifs*0.5860 + expressions*0.3139 + 0.3851

    #y_abs
    # pred = longueur_infavor*0.5609 + ponctuation*0.1340 + negation*-0.6350 + adverbes_negatifs*0.5854 + expressions*0.3144 + 0.1427

in_comment = False
comment = {}

with open("datasets/train_data.tsv", mode="w") as output:
    tsv_output = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    tsv_output.writerow(["comment_id", "longueur_infavor", "ponctuation", "negation", "adverbes_negatifs", "expressions", "alignment", "prediction"])
    for line in sys.stdin:
        line = line.rstrip("\n")
        if line.startswith('<comment_'):    
            in_comment = True
            match = re.search(r"<comment_(\d+) alignment=\'(In favor|Against)\' langue=\'(..)\'>", line)
            if match:
                comment["id"] = match.group(1)
                comment["alignment"] = match.group(2)
                comment["language"] = match.group(3)
                comment["text"] = ""
                comment["tokens"] = list()
        elif line.startswith('</comment_'):

            tsv_output.writerow([comment["id"], 
                                 proba_infavor(comment["tokens"]),
                                 ponctuation_score(comment["text"]),
                                 nombre_de_neg(comment["tokens"])[0],
                                 nombre_de_neg(comment["tokens"])[1],
                                 score_expression(comment["text"], comment["language"]),
                                 comment["alignment"],
                                 prediction(proba_infavor(comment["tokens"]),
                                    ponctuation_score(comment["text"]),
                                    nombre_de_neg(comment["tokens"])[0],
                                    nombre_de_neg(comment["tokens"])[1],
                                    score_expression(comment["text"], comment["language"]))])

            in_comment = False
            comment = {}

        elif line.startswith('# text ='):
            comment["text"] += (re.search(r"^# text = (.*)$", line).group(1))

        else:
            line = line.split("\t")
            if len(line) == 10:
                comment["tokens"].append(line)