import sys
from datasets import data_by_lang
import pandas as pd
import stanza
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer
from transformers import pipeline


def en_sentiment_analyse(data):
    """
    from a list of text, it returns the sentiment analyse using a Stanza english model
    :param data: list with elements
    :return: list with sentiment element as output of the english stanza sentiment property from an analyse models.
            more information: https://stanfordnlp.github.io/stanza/data_objects.html#sentence
    """
    nlp_sentiment = []
    nlp = stanza.Pipeline(
        lang="en", processors="tokenize,sentiment", tokenize_no_ssplit=True
    )
    for comment in data:
        doc = nlp(comment)
        phrase_sentiment = []
        for i, sentence in enumerate(doc.sentences):
            phrase_sentiment.append(sentence.sentiment)
        nlp_sentiment.append(phrase_sentiment)
    analyses_list = [ana for ana_list in nlp_sentiment for ana in ana_list]
    return nlp_sentiment


def fr_sentiment_analyse(data):
    """
    from a list of text, it returns the polarity from a Sentiment property using a TextBlob french model to sentiment analyse
    :param data: list with str elements in french
    :return: two lists; one with the sentiment analyse (polarity; -1 to 1) and other with the subjectivity (from 0.0 to 1.0).
    more infor here: https://textblob.readthedocs.io/en/dev/api_reference.html#textblob.blob.TextBlob.sentiment
    """
    sentiment_analyse = []
    subjectivity = []
    for comment in data:
        blob = TextBlob(comment, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
        sentiment_analyse.append(round(blob.sentiment[0], 2))
        subjectivity.append(round(blob.sentiment[1], 2))
    return sentiment_analyse, subjectivity


def es_sentiment_analyse(data):
    model_path = "edumunozsala/roberta_bne_sentiment_analysis_es"
    sent_analyser = pipeline(
        "text-classification", model=model_path, tokenizer=model_path
    )
    sentiment_analyse = []
    for comment in data:
        analyse = sent_analyser(comment)
        sentiment_analyse.append(analyse[0]["label"])
    return sentiment_analyse


def analyse_par_lang(path_data, lang):
    if lang == "en":
        data = data_by_lang.filter_data_by_language(path_data, lang)
        en_sent_analyse = en_sentiment_analyse(data)
        return en_sent_analyse
    elif lang == "fr":
        data = data_by_lang.filter_data_by_language(path_data, lang)
        fr_sent_analyse = fr_sentiment_analyse(data)
        return fr_sent_analyse
    elif lang == "es":
        data = data_by_lang.filter_data_by_language(path_data, lang)
        es_sent_analyse = data_by_lang.filter_data_by_language(data, "es")
        data_by_lang.full_data_by_language("datasets/CFS.tsv", "es")
        return es_sent_analyse
    elif lang == "all":
        en_data = data_by_lang.full_data_by_language(path_data, "en")
        en_filtered_data = data_by_lang.filter_data_by_language(path_data, "en")
        en_sent_analyse = en_sentiment_analyse(en_filtered_data)
        en_data["en_sent_analyse"] = en_sent_analyse
        es_filtered_data = data_by_lang.filter_data_by_language(path_data, "es")
        es_data = data_by_lang.full_data_by_language(es_filtered_data, "es")
        es_sent_analyse = es_sentiment_analyse(es_filtered_data)
        es_data["es_sent_analyse"] = es_sent_analyse
        fr_filtered_data = data_by_lang.filter_data_by_language(path_data, "fr")
        fr_data = data_by_lang.full_data_by_language(fr_filtered_data, "fr")
        fr_sent_analyse = fr_sentiment_analyse(path_data)
        fr_data["fr_sent_analyse"] = fr_sent_analyse
        full_analysed_data = pd.concat([fr_data, en_data, es_data])
        return full_analysed_data
    else:
        return None
